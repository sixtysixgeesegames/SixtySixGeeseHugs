﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// A monobehaviour object to house an instance of <see cref="DataManager"/> so it can be called by other objects
/// </summary>
public class PlayerData : MonoBehaviour {

	public DataManager data;
	public static PlayerData instance = null;

	/// <summary>
	/// Creates <see cref="DataManager"/> object and adds initial player 
	/// </summary>
	void Awake() {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else if (instance != this) {
			Destroy (gameObject);
		}
	}

}

/// <summary>
/// An object to store all player data including players, items and money and provide useful functions
/// </summary>
public class DataManager {

	private Player[] players;
	private Item[] items;
	private int money;
    private bool showQuest;

    private Player swappedPlayer; 

	public DataManager(Player initialPlayer) {
		players = new Player[6];
		players [0] = initialPlayer;
		items = new Item[6];
		money = 0;
        showQuest = true;
        swappedPlayer = null;
	}

	public Player[] Players {
		get {
			return this.players;
		}
		set {
			players = value;
		}
	}

	public Item[] Items {
		get {
			return this.items;
		}
	}

	public int Money {
		get {
			return this.money;
		}
		set {
			money = value;
		}
	}

    public bool ShowQuest
    {
        get
        {
            return this.showQuest;
        }
        set
        {
            showQuest = value;
        }
    }

    /// <summary>
    /// Gets the first player.
    /// </summary>
    /// <returns>The first player in the array, <see cref="players"/>[0] </returns>
    public Player getFirstPlayer() {
		return players [0];
	}

	/// <summary>
	/// Returns the number of players in <see cref="players"/> that are not null and have health above zero 
	/// </summary>
	/// <returns>The number of players alive</returns>
	public int playersAlive() {
		int alive = 0;
		foreach (Player player in players) {
			if (player != null) {
				if (player.Health > 0) {
					alive += 1;
				}
			}
		}
		return alive;
	}

	/// <summary>
	/// Adds a new player to <see cref="players"/> if not-full, otherwise throwing an <c> InvalidOperationException</c>
	/// </summary>
	/// <param name="player">The player to add to the array</param>
	public void addPlayer(Player player) {
		bool added = false;
		for (int i = 0; i < players.Length; i++) {
			if (players[i] == null) {
				players[i] = player;
				added = true;
				break;
			}
		}
		if (!added) {
			Debug.Log("Player Array is full");
		}
	}
    /// <summary>
    /// function for getting a pointer to the highest level player
    /// </summary>
    /// <returns>an integer pointing to the highest level</returns>
    private int getHighestLevel()
    {
        int highestlevel = 0;
        int highest = 0;
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] != null && players[i].Level > highestlevel)
            {
                highest = i;
                highestlevel = players[i].Level;
            }

        }
        return highestlevel;
    }
    /// <summary>
    /// a function that swaps the lowest level player into the garilla 
    /// </summary>
    public void SwapLowestPlayerToGorilla()
    {
        //find lowest level
        int lowestlevel = 999;
        int lowest = 0;
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] != null && players[i].Level < lowestlevel)
            {
                lowest = i;
                lowestlevel = players[i].Level;
            }

        }
        //swap with a gorilla
        Player newPlayer = new Player("Raging Gorilla", 1, 100, 30, 30, 25, 25, 25, 30, 0, null,
                new RaiseAttack("Beat Chest", "increse your attack Power with primal music", 5, 0.5f),
                new RageAttack("spun wildley towards", "Unpredictable devastating attack", 9, 40),
                (Texture2D)Resources.Load("CharacterMonkey", typeof(Texture2D)));
        for (int i=0;i<getHighestLevel();i++)
        {
            newPlayer.levelUp();
        }
        //saves the swapped player into a variable
        swappedPlayer = players[lowest];

        players[lowest] = newPlayer;

    }
    /// <summary>
    /// changes the Gorilla back into the temporary character
    /// </summary>
    public void RevertGorilla()
    {
        int gorillapos = 0;
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i] != null)
            {
                if (players[i].Name == "Raging Gorilla")
                    gorillapos = i;
            }
        }
        players[gorillapos] = swappedPlayer;
    }
    /// <summary>
    /// Returns the number of Players
    /// </summary>
    /// <returns>number of players int</returns>
    public int GetNumberOfPlayers()
    {
        return players.Length;
    }

	/// <summary>Swap two player's positions in <see cref="players"/></summary>
	/// <param name="index1">The index of one player to swap</param>
	/// <param name="index2">The index of the other player to swap</param>
	public void swapPlayers(int index1, int index2) {
		Player temp = players [index1];
		players [index1] = players [index2];
		players [index2] = temp;
	}

	/// <summary>
	/// Add an item to <see cref="items"/> 
	/// </summary>
	/// <param name="item">The item to add</param>
	public void addItem(Item item) {
		for (int i = 0; i < items.Length; i++) {
			if (items [i] == null) {
                if (QuestStorage.instance.DoesQuestTypeExist(2))
                    if (GlobalFunctions.instance.getItems()[QuestStorage.instance.GetGoalWithType(2)] == item.Name)
                        QuestStorage.instance.SetProgressWithType(2, 1);
				items [i] = item;
				break;
			}
		}
	}

    /// <summary>
	/// Removes an item to <see cref="items"/> 
	/// </summary>
	/// <param name="item">The item to remove</param>
	public void removeItem(Item item)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] != null)
            {
                if (items[i].GetType() == item.GetType())
                {
                    items[i] = null;
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Counts all items in <see cref="items"/> which are not null
    /// </summary>
    /// <returns>The number of non-null elements in the array</returns>
    public int countItems() {
		int count = 0;
		for (int i = 0; i < items.Length; i++) {
			if (items [i] != null) {
				count += 1;
			}
		}
		return count;
	}


    /// <summary>
    /// converts players into stored data versions for saving
    /// </summary>
    /// <param name="player"></param>
    /// <returns></returns>
    public StoredData.Player getStoredFromPlayer(Player player)
    {
        StoredData.Player playerToStore = new StoredData.Player();
        playerToStore.name = player.Name;
        playerToStore.level = player.Level;
        playerToStore.health = player.Health;
        playerToStore.attack = player.Attack;
        playerToStore.defence = player.Defence;
        playerToStore.maximumMagic = player.MaximumMagic;
        playerToStore.magic = player.Magic;
        playerToStore.luck = player.Luck;
        playerToStore.speed = player.Speed;
        playerToStore.exp = player.Exp;
        if (player.Item != null)
        {
            playerToStore.item = new StoredData.Item();
            playerToStore.item.name = player.Item.Name;
        }
        playerToStore.special1 = getMoveFromPlayer(player.Special1);
        playerToStore.special2 = getMoveFromPlayer(player.Special2);

        if (player.Image != null)
        {
            playerToStore.imageDims = new int[2];
            playerToStore.imageDims[0] = player.Image.width;
            playerToStore.imageDims[1] = player.Image.height;
            playerToStore.textureArray = player.Image.EncodeToPNG();
        }

        return playerToStore;
    }
    /// <summary>
    /// converts items into storable versions
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public StoredData.Item getStoredFromItem(Item item)
    {
        StoredData.Item itemToStore = new StoredData.Item();
        itemToStore.name = item.Name;
        return itemToStore;
    }
    /// <summary>
    /// Converts location data into a storable form
    /// </summary>
    /// <param name="playerObject"></param>
    /// <param name="sceneName"></param>
    /// <param name="level"></param>
    /// <returns></returns>
    public StoredData.LocationData getStoredFromLocationData(GameObject playerObject, string sceneName, int level)
    {
        StoredData.LocationData locationToStore = new StoredData.LocationData();
        locationToStore.playerLocationX = playerObject.transform.position.x;
        locationToStore.playerLocationY = playerObject.transform.position.y;

        locationToStore.sceneName = sceneName;
        locationToStore.currentLevel = level;
        return locationToStore;
    }

    

    /// <summary>
    /// Collects all of the data and maps it to my own serializable classes and then puts them into the savefile
    /// </summary>
    public void Save()
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/saveFile.dat");

        StoredData storedData = new StoredData();

        for (int i = 0; i < players.Length; i++)
        {
            // player stuff
            if (players[i] != null)
            {
                storedData.players[i] = getStoredFromPlayer(players[i]);
            }
        }
        if(swappedPlayer!=null)
            storedData.swappedPlayer = getStoredFromPlayer(PlayerData.instance.data.swappedPlayer);

        // items
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] != null)
            {
                storedData.items[i] = getStoredFromItem(items[i]);
            }
        }

        // money
        storedData.money = money;

        storedData.showQuest = showQuest;

        

        // player location
        GameObject playerObject = GameObject.Find("Player");
        storedData.locationData = getStoredFromLocationData(playerObject, SceneManager.GetActiveScene().name,
            GlobalFunctions.instance.currentLevel);

        // objects active
        storedData.objectsActive = (Dictionary<string, bool>)GlobalFunctions.instance.objectsActive; 

        // quests
        storedData.activeQuests = QuestStorage.instance.ActiveQuests;

        binaryFormatter.Serialize(file, storedData);
        Debug.Log("Saved");
        file.Close();
    }

    public Player getPlayerFromStored(StoredData.Player storedPlayer)
    {
        Player playerToLoad;
        Texture2D image = new Texture2D(storedPlayer.imageDims[0], storedPlayer.imageDims[1]);
        image.LoadImage(storedPlayer.textureArray);
        if (storedPlayer.item != null)
        {
            playerToLoad = new Player(storedPlayer.name, storedPlayer.level, storedPlayer.health, storedPlayer.attack, storedPlayer.defence, storedPlayer.maximumMagic, storedPlayer.magic, storedPlayer.luck, storedPlayer.speed, storedPlayer.exp, getItemFromStored(storedPlayer.item), getMoveFromStored(storedPlayer.special1), getMoveFromStored(storedPlayer.special2), image);
        }
        else
        {
            playerToLoad = new Player(storedPlayer.name, storedPlayer.level, storedPlayer.health, storedPlayer.attack, storedPlayer.defence, storedPlayer.maximumMagic, storedPlayer.magic, storedPlayer.luck, storedPlayer.speed, storedPlayer.exp, null, getMoveFromStored(storedPlayer.special1), getMoveFromStored(storedPlayer.special2), image);
        }

        return playerToLoad;
    }


    public Vector2 getVectorFromLocationData(StoredData.LocationData locationData)
    {
        return new Vector2(locationData.playerLocationX, locationData.playerLocationY);
    }

    public String getSceneNameFromLocationData(StoredData.LocationData locationData)
    {
        return locationData.sceneName;
    }

    public int getLevelFromLocationData(StoredData.LocationData locationData)
    {
        return locationData.currentLevel;
    }


    /// <summary>
        /// Gets all saved data mapped to my classes and loads it into the game
        /// </summary>
        public void Load()
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/saveFile.dat", FileMode.Open);
        StoredData storedData = (StoredData)binaryFormatter.Deserialize(file);

        // players
        for(int i = 0; i < storedData.players.Length; i++)
        {
            StoredData.Player player = storedData.players[i];
            if(player != null)
            {
                Texture2D image = new Texture2D(player.imageDims[0], player.imageDims[1]);
                image.LoadImage(player.textureArray);
                addPlayer(getPlayerFromStored(player));
            }
        }
        StoredData.Player temp = storedData.swappedPlayer;
        if(temp!=null)
        {
            Texture2D image = new Texture2D(temp.imageDims[0], temp.imageDims[1]);
            image.LoadImage(temp.textureArray);
            PlayerData.instance.data.swappedPlayer = getPlayerFromStored(temp);
        }



        // items
        for(int i = 0; i < storedData.items.Length; i++)
        {
            StoredData.Item item = storedData.items[i];
            if(item != null)
            {
                items[i] = getItemFromStored(item);
            }
        }

        money = storedData.money;

        showQuest = storedData.showQuest;

        // current level
        GlobalFunctions.instance.currentLevel = getLevelFromLocationData(storedData.locationData);

        // active objects
        GlobalFunctions.instance.objectsActive = storedData.objectsActive;

        // active quests
        QuestStorage.instance.ActiveQuests = storedData.activeQuests;

        // load level
        SceneChanger.instance.loadLevel(getSceneNameFromLocationData(storedData.locationData), getVectorFromLocationData(storedData.locationData));

        file.Close();

    }


    /// <summary>
    /// Serializable classes storing save data
    /// </summary>
    [Serializable]
    public class StoredData
    {

        public int money;

        [Serializable]
        public class SpecialMove
        {
            public string text;
            public string desc;
            public int magic;
        }

        [Serializable]
        public class MagicAttack : SpecialMove
        {
            public int power;
        }

        [Serializable]
        public class LowerDefence : SpecialMove
        {
            public float decrease;
        }

        [Serializable]
        public class LowerSpeed : SpecialMove
        {
            public float decrease;
        }

        [Serializable]
        public class RaiseAttack : SpecialMove
        {
            public float increase;
        }

        [Serializable]
        public class RaiseDefence : SpecialMove
        {
            public float increase;
        }

        [Serializable]
        public class IncreaseMoney : SpecialMove
        {
            public float increase;
        }

        [Serializable]
        public class HealingSpell : SpecialMove
        {
            public int increase;
        }

        [Serializable]
        public class RageAttack : SpecialMove
        {
            public int power;
        }


        [Serializable]
        public class Item
        {
            public string name;
        }

        [Serializable]
        public class Player
        {
            public string name;
            public int level;
            public int health;
            public int attack;
            public int defence;
            public int maximumMagic;
            public int magic;
            public int luck;
            public int speed;
            public int exp;
            public Item item;
            public SpecialMove special1;
            public SpecialMove special2;
            public byte[] textureArray;
            public int[] imageDims;
        }

        [Serializable]
        public class LocationData
        {
            public string sceneName;
            public float playerLocationX;
            public float playerLocationY;
            public int currentLevel;
        }

        public Player[] players = new Player[6];
        public Item[] items = new Item[6];
        public LocationData locationData = new LocationData();
        public Dictionary<string, bool> objectsActive = new Dictionary<string, bool>();
        public int[,] activeQuests;
        public Player swappedPlayer = null;
        public bool showQuest;

    }

    /// <summary>
    /// Takes the saved data move and returns the correct method needed for the game
    /// </summary>
    /// <param name="specialMove">The stored move</param>
    /// <returns>Move used for game</returns>
    public SpecialMove getMoveFromStored(StoredData.SpecialMove specialMove)
    {
        if(specialMove is StoredData.MagicAttack)
        {
            MagicAttack magicAttack = new MagicAttack(specialMove.text, specialMove.desc, specialMove.magic, ((StoredData.MagicAttack)specialMove).power);
            return magicAttack;
        }
        else if(specialMove is StoredData.LowerDefence)
        {
            LowerDefence lowerDefence = new LowerDefence(specialMove.text, specialMove.desc, specialMove.magic, ((StoredData.LowerDefence)specialMove).decrease);
            return lowerDefence;
        }
        else if(specialMove is StoredData.LowerSpeed)
        {
            LowerSpeed lowerSpeed = new LowerSpeed(specialMove.text, specialMove.desc, specialMove.magic, ((StoredData.LowerSpeed)specialMove).decrease);
            return lowerSpeed;
        }
        else if(specialMove is StoredData.RaiseAttack)
        {
            RaiseAttack raiseAttack = new RaiseAttack(specialMove.text, specialMove.desc, specialMove.magic, ((StoredData.RaiseAttack)specialMove).increase);
            return raiseAttack;
        }
        else if(specialMove is StoredData.RaiseDefence)
        {
            RaiseDefence raiseDefence = new RaiseDefence(specialMove.text, specialMove.desc, specialMove.magic, ((StoredData.RaiseDefence)specialMove).increase);
            return raiseDefence;
        }
        else if(specialMove is StoredData.IncreaseMoney)
        {
            IncreaseMoney increaseMoney = new IncreaseMoney(specialMove.text, specialMove.desc, specialMove.magic, ((StoredData.IncreaseMoney)specialMove).increase);
            return increaseMoney;
        }
        else if(specialMove is StoredData.HealingSpell)
        {
            HealingSpell healingSpell = new HealingSpell(specialMove.text, specialMove.desc, specialMove.magic, ((StoredData.HealingSpell)specialMove).increase);
            return healingSpell;
        }
        else if(specialMove is StoredData.RageAttack)
        {
            RageAttack rageattack = new RageAttack(specialMove.text, specialMove.desc, specialMove.magic, ((StoredData.RageAttack)specialMove).power);
            return rageattack;
        }
        return null;
    }

    /// <summary>
    /// Gets the move in the game and returns a move that is able to be saved
    /// </summary>
    /// <param name="specialMove">The special move from the player to convert it to be stored</param>
    /// <returns>Storable item</returns>
    public StoredData.SpecialMove getMoveFromPlayer(SpecialMove specialMove)
    {
        if (specialMove is MagicAttack)
        {
            StoredData.MagicAttack move = new StoredData.MagicAttack();
            move.desc = specialMove.Desc;
            move.magic = specialMove.Magic;
            move.power = ((MagicAttack)specialMove).power;
            move.text = specialMove.Text;
            return move;
        }
        else if (specialMove is LowerDefence)
        {
            StoredData.LowerDefence move = new StoredData.LowerDefence();
            move.desc = specialMove.Desc;
            move.magic = specialMove.Magic;
            move.decrease = ((LowerDefence)specialMove).decrease;
            move.text = specialMove.Text;
            return move;
        }
        else if (specialMove is LowerSpeed)
        {
            StoredData.LowerSpeed move = new StoredData.LowerSpeed();
            move.desc = specialMove.Desc;
            move.magic = specialMove.Magic;
            move.decrease = ((LowerSpeed)specialMove).decrease;
            move.text = specialMove.Text;
            return move;
        }
        else if (specialMove is RaiseAttack)
        {
            StoredData.RaiseAttack move = new StoredData.RaiseAttack();
            move.desc = specialMove.Desc;
            move.magic = specialMove.Magic;
            move.increase = ((RaiseAttack)specialMove).increase;
            move.text = specialMove.Text;
            return move;
        }
        else if (specialMove is RaiseDefence)
        {
            StoredData.RaiseDefence move = new StoredData.RaiseDefence();
            move.desc = specialMove.Desc;
            move.magic = specialMove.Magic;
            move.increase = ((RaiseDefence)specialMove).increase;
            move.text = specialMove.Text;
            return move;
        }
        else if (specialMove is IncreaseMoney)
        {
            StoredData.IncreaseMoney move = new StoredData.IncreaseMoney();
            move.desc = specialMove.Desc;
            move.magic = specialMove.Magic;
            move.increase = ((IncreaseMoney)specialMove).increase;
            move.text = specialMove.Text;
            return move;
        }
        else if (specialMove is HealingSpell)
        {
            StoredData.HealingSpell move = new StoredData.HealingSpell();
            move.desc = specialMove.Desc;
            move.magic = specialMove.Magic;
            move.increase = ((HealingSpell)specialMove).increase;
            move.text = specialMove.Text;
            return move;
        }
        else if (specialMove is RageAttack)
        {
            StoredData.RageAttack move = new StoredData.RageAttack();
            move.desc = specialMove.Desc;
            move.magic = specialMove.Magic;
            move.power = ((RageAttack)specialMove).power;
            move.text = specialMove.Text;
            return move;
        }

        return null;
    }

    /// <summary>
    /// Gets item from name
    /// </summary>
    /// <param name="itemName">The item name</param>
    /// <returns>An item instance</returns>
    public Item getItemFromStored(StoredData.Item item)
    {
        switch (item.name)
        {
            case "Hammer":
                return new Hammer();
            case "Trainers":
                return new Trainers();
            case "Rabbit Foot":
                return new RabbitFoot();
            case "Magic Amulet":
                return new MagicAmulet();
            case "Shield":
                return new Shield();
            case "Armour":
                return new Armour();
            case "Golden Armour":
                return new GoldenArmour();
            case "Sword":
                return new Sword();
            case "Magic Sword":
                return new MagicSword();
            case "Nikes Premium Shoes":
                return new NikesPremiumShoes();
            case "Rabbit carcas":
                return new RabbitCarcass();
            case "Magic Hat":
                return new MagicHat();
            case "Diamond Shield":
                return new DiamondShield();
            case "Monkeys Paw":
                return new MonkeysPaw();
        }
        return null;
    }



}
