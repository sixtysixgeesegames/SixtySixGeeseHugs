﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

[TestFixture]
public class GorillaFeatureTesting
{
    private Player playerObject;
    private Player swapPlayerObject;
    private DataManager data;

    [SetUp]
    public void GorillaFeatureTestingSimplePasses()
    {
        playerObject = new Player("George", 5, 100, 30, 5, 5, 5, 5, 5, 0, null, null, null);
        PlayerData.instance = new PlayerData();
        data = new DataManager(playerObject);
        PlayerData.instance.data = data;
        swapPlayerObject = new Player("LowLevelPlayer", 1, 100, 30, 5, 5, 5, 5, 5, 0, null, null, null);
        data.addPlayer(swapPlayerObject);
    }

    [Test]
    public void TestGorillaSwappedIn()
    {
        data.SwapLowestPlayerToGorilla();
        Assert.AreEqual(data.Players[1].Name, "Raging Gorilla");
    }

    [Test]
    public void TestPlayerReverted()
    {
        data.RevertGorilla();
        Debug.Log(data.Players[1]);
        Debug.Log(swapPlayerObject);
        Assert.AreEqual(data.Players[1],swapPlayerObject);
    }

    [Test]
    public void GorillaSpecialAttackNormal()
    {

        // Gorilla and enemy setup. Rage attack forced to attack enemy.
        int movePower = 10;
        Player gorilla = new Player("Raging Gorilla", 1, 100, 10, 30, 25, 25, 25, 30, 0, null,
            new RaiseAttack("Beat Chest", "increse your attack Power with primal music", 5, 0.5f),
            new RageAttack("spun wildley towards", "Unpredictable devastating attack", 9, movePower, false, true),
            null);

        Enemy enemyObject = new Enemy("Enemy", 10, 100, 10, 10, 10, 10, 10, 10, new MagicAttack("fireballed", "Fireball", 30, 5), new MagicAttack("fireballed", "Fireball", 30, 10));

        int oldEnemyHealth = enemyObject.Health;
        int oldGorillaMagic = gorilla.Magic;

        // BattleManager Setup
        BattleManager battleManager= new BattleManager(gorilla, enemyObject, 50);
        Player player = battleManager.Player;
        Enemy enemy = battleManager.Enemy;
        battleManager.forceCriticalHits = "None";

        gorilla.Special2.setUp(battleManager, player, enemy);
        gorilla.Special2.performMove();

        Assert.AreEqual(oldEnemyHealth - battleManager.damageCalculation(battleManager.Player, battleManager.Enemy, movePower*2), enemy.Health);
        Assert.AreEqual(oldGorillaMagic - player.Special2.Magic, player.Magic);
    }

    [Test]
    public void GorillaSpecialAttackFriendlyFire()
    {
        int movePower = 10;
        Player gorilla = new Player("Raging Gorilla", 1, 100, 10, 30, 25, 25, 25, 30, 0, null,
            new RaiseAttack("Beat Chest", "increse your attack Power with primal music", 5, 0.5f),
            new RageAttack("spun wildley towards", "Unpredictable devastating attack", 9, movePower, true),
            null);

        int oldGorillaMagic = gorilla.Magic;

        Enemy enemyObject = new Enemy("Enemy", 10, 100, 10, 10, 10, 10, 10, 10, new MagicAttack("fireballed", "Fireball", 30, 5), new MagicAttack("fireballed", "Fireball", 30, 10));


        // BattleManager Setup
        BattleManager battleManager = new BattleManager(gorilla, enemyObject, 50);
        Player player = battleManager.Player;
        Enemy enemy = battleManager.Enemy;
        battleManager.forceCriticalHits = "None";

        // Create a full dummy team, to ensure someone gets hit.
        for (int i = 0; i < 5; i++)
        {
            PlayerData.instance.data.addPlayer(new Player("George" + i, 5, 100, 30, 5, 5, 5, 5, 5, 0, null, null, null));
        }

        int initialTeamHealth = 0;

        // Calculate teams' health.
        for (int i = 0; i < 6; i++)
        {
            initialTeamHealth += PlayerData.instance.data.Players[i].Health;
        }

        gorilla.Special2.setUp(battleManager, player, enemy);
        gorilla.Special2.performMove();

        // Calculate teams' health after attack.
        int teamHealth = 0;
        for (int i = 0; i < 6; i++)
        {
            teamHealth += PlayerData.instance.data.Players[i].Health;
        }

        Assert.AreEqual(initialTeamHealth - 50, teamHealth);
        Assert.AreEqual(oldGorillaMagic - player.Special2.Magic, player.Magic);


    }
}