﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

[TestFixture]
public class SaveLoadTesting {

    [SetUp]
    public void Init()
    {
        PlayerData.instance = new PlayerData();
        PlayerData.instance.data = new DataManager(new Player("George", 1, 100, 5, 5, 5, 5, 5, 5, 0, new Hammer(),
            new MagicAttack("hi-jump kicked", "Kick with power 15", 3, 15),
            new RaiseDefence("buffed up against", "Increase your defence by 10%", 2, 0.1f),
            (Texture2D)Resources.Load("Character1", typeof(Texture2D))));
    }

    [Test]
    public void SaveLoadPlayer()
    {
        Player testPlayer = new Player("George", 1, 100, 5, 5, 5, 5, 5, 5, 0, new Hammer(),
            new MagicAttack("hi-jump kicked", "Kick with power 15", 3, 15),
            new RaiseDefence("buffed up against", "Increase your defence by 10%", 2, 0.1f),
            (Texture2D)Resources.Load("Character1", typeof(Texture2D)));

        DataManager.StoredData.Player savedPlayer = PlayerData.instance.data.getStoredFromPlayer(testPlayer);
        Player restoredPlayer = PlayerData.instance.data.getPlayerFromStored(savedPlayer);
        Assert.AreEqual(restoredPlayer.Name, testPlayer.Name);
        Assert.AreEqual(restoredPlayer.Level, testPlayer.Level);
        Assert.AreEqual(restoredPlayer.Health, testPlayer.Health);
        Assert.AreEqual(restoredPlayer.Attack, testPlayer.Attack);
        Assert.AreEqual(restoredPlayer.Defence, testPlayer.Defence);
        Assert.AreEqual(restoredPlayer.MaximumMagic, testPlayer.MaximumMagic);
        Assert.AreEqual(restoredPlayer.Magic, testPlayer.Magic);
        Assert.AreEqual(restoredPlayer.Luck, testPlayer.Luck);
        Assert.AreEqual(restoredPlayer.Speed, testPlayer.Speed);
        Assert.AreEqual(restoredPlayer.Exp, testPlayer.Exp);

        Assert.AreEqual(restoredPlayer.Image.EncodeToPNG(), testPlayer.Image.EncodeToPNG());

        // Other non-primitive data types are tested below.
    }

    [Test]
    public void SaveLoadItem()
    {
        Item testItem = new Hammer();

        DataManager.StoredData.Item savedItem = PlayerData.instance.data.getStoredFromItem(testItem);
        Item restoredItem = PlayerData.instance.data.getItemFromStored(savedItem);

        Assert.IsInstanceOf<Hammer>(restoredItem);
        Assert.AreEqual(testItem.Name, restoredItem.Name);
        Assert.AreEqual(testItem.Desc, restoredItem.Desc);
    }

    [Test]
    public void SpecialMoveSaveLoad()
    {
        MagicAttack testMove = new MagicAttack("testtext", "testdesc", 1, 1);

        DataManager.StoredData.SpecialMove savedMove = PlayerData.instance.data.getMoveFromPlayer(testMove);
        MagicAttack restoredMove = (MagicAttack)PlayerData.instance.data.getMoveFromStored(savedMove);

        Assert.IsInstanceOf<MagicAttack>(restoredMove);
        Assert.AreEqual(testMove.Desc, restoredMove.Desc);
        Assert.AreEqual(testMove.Text, restoredMove.Text);
        Assert.AreEqual(testMove.Magic, restoredMove.Magic);
        Assert.AreEqual(testMove.power, restoredMove.power);
    }

    [Test]
    public void LocationDataSaveLoad()
    {
        Vector2 testVector = new Vector2(5, 5);

        GameObject playerObject = new GameObject();
        playerObject.transform.SetPositionAndRotation(testVector, Quaternion.identity);

        DataManager.StoredData.LocationData locationData = PlayerData.instance.data.getStoredFromLocationData(playerObject, "testscene", 0);
        Vector2 restoredVector = PlayerData.instance.data.getVectorFromLocationData(locationData);
        string scenename = PlayerData.instance.data.getSceneNameFromLocationData(locationData);
        int levelnumber = PlayerData.instance.data.getLevelFromLocationData(locationData);

        Assert.AreEqual(testVector, restoredVector);
        Assert.AreEqual(scenename, "testscene" );
        Assert.AreEqual(levelnumber, 0);
    }
}
