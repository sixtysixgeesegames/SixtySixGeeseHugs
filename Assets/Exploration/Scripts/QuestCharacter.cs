﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCharacter : MonoBehaviour {
    public int questType;
    public int questNumber;
    public bool Quest = true;

    // Use this for initialization
    void Start()
    {
        //if a fighting quest destroy the object if the quest doesn't exist
        if (Quest)
        {
            if (QuestStorage.instance.DoesQuestTypeExist(questType))
            {
                if (QuestStorage.instance.GetGoalWithType(questType) == questNumber)
                {
                    //stay alive
                }
                else
                    Destroy(transform.parent.gameObject);
            }
            else
                Destroy(transform.parent.gameObject);
        }
        
    }
	//update the quest when callled or perform a special action
public void updateQuest()
    {
        if (Quest)
            QuestStorage.instance.SetProgressWithType(questType, 1);
        else
        {
            PlayerData.instance.data.SwapLowestPlayerToGorilla();
            this.gameObject.SetActive(false);
        }

    }
}
