﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestSelector : MonoBehaviour {



    /// <summary>How many quest choices for the first option to have </summary>
    public int numberOfItems = 3;
    /// <summary>How many items you want per row of the interface </summary>
    public int numberOfItemsPerRow = 1;
    //cell width and height are both recalculated on start 
    private float cellWidth = Screen.width / 4;
    private float cellHeight = Screen.height / 3;
    /// <summary>the images you want to display with the items (order matters) </summary>
    public Texture tex;
    public Texture tex2;
    public Texture bg;
    /// <summary>the cost you want to display with the items (order matters) </summary>
    /// <summary>the items you want to display (order matters) </summary>
    /// <summary>if the shop should be drawing or not </summary>
    private bool draw = true;

    private int difficulty = 0;
    private int maxDifficulty = 2;
    private int state = -1;
    /*
        type:
           0:get gold
           1:defeat all enemies in area
           2:get item 
           3:get score in minigame
           4:beat number of enemies only using basic
           12(5): beat a certain person
           5:only kill required enemies
           6:defeat every enemy
           7:never spend money
           8:never equip an item
           9:speak to everyone
           10:don't let anyone die
           11:only save and exit when enabled

        NEED TO MAKE IT ONLY ALLOW ONE OF EACH SO CAN'T HAVE GET 100 GOlD, GET 250 GOLD AND GET 500 GOLD!!!!

         */
    private int[,] optionquests;
    private int[,] optionruns;
    private bool[] selectoptions;

    private void Start()
    {
        //resize the cells to fit in screen
        cellHeight = Screen.height / (numberOfItems / numberOfItemsPerRow + 1);
        cellWidth = Screen.width / (numberOfItemsPerRow + 1);
        optionquests = new int[3, 3];
        optionruns = new int[8, 3];
        selectoptions = new bool[7];
        GenerateQuest(difficulty);

        if (PlayerData.instance.data.ShowQuest)
            state = 0;
        else
            draw = false;

    }


    // Update is called once per frame
    void FixedUpdate()
    {

        //check which cell clicked on, on mouse press LEFT and perform the action if money allows 
        if (Input.GetMouseButtonDown(0))
        {
            //different click for each state
            //state 0 is 3 choices of 3 randomly designed quests
            if (state == 0)
            {
                for (int i = 0; i < numberOfItems; i++)
                {
                    Rect bounds = new Rect(i % numberOfItemsPerRow * (cellWidth + 5), Mathf.Round(i / numberOfItemsPerRow) * (cellHeight + 5), cellWidth, cellHeight);

                    if (bounds.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
                    {

                        CreateQuest(optionquests[i, 0], optionquests[i, 1], optionquests[i, 2]);
                        difficulty += 1;
                        GenerateQuest(difficulty);
                        if (difficulty > maxDifficulty)
                        {

                            numberOfItems = 7;
                            cellHeight = Screen.height / (numberOfItems / numberOfItemsPerRow + 1);
                            cellWidth = Screen.width / (numberOfItemsPerRow + 1);
                            GenerateRuns();
                            state = 1;
                        }
                        return;
                    }
                }
            }
            //state 1 is a list of runs that they can toggle on and off before confirming
            else if (state == 1)
            {
                Rect finish = new Rect(1 * (cellWidth + 5), 2 * cellHeight, cellWidth, cellHeight);
                if (finish.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
                    {

                        for (int i=0;i<selectoptions.Length;i++)
                    {
                        if(selectoptions[i])
                            CreateQuest(optionruns[i, 0], optionruns[i, 1], optionruns[i, 2]);
                    }
                    draw = false;
                    state = 3;
                    
                    PlayerData.instance.data.ShowQuest = false;
                    this.gameObject.SetActive(false);

                }
                        for (int i = 0; i < numberOfItems; i++)
                {
                    Rect bounds = new Rect(i % numberOfItemsPerRow * (cellWidth + 5), Mathf.Round(i / numberOfItemsPerRow) * (cellHeight + 5), cellWidth, cellHeight);

                    if (bounds.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
                    {
                        if (selectoptions[i]==false)
                            selectoptions[i] = true;
                        else
                            selectoptions[i] = false;
                    }
                }
            }
        }
       

    }

    void OnGUI()
    {
        // if can draw, draw out the options of the quest selection
        if (draw)
        {
            //draw background
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), bg);
            //draw the 3 options and difficulty
            if (state == 0)
            {
                for (int i = 0; i < numberOfItems; i++)
                {
                    Rect bounds = new Rect(i % numberOfItemsPerRow * (cellWidth + 5), Mathf.Round(i / numberOfItemsPerRow) * (cellHeight + 5), cellWidth, cellHeight);
                    Rect text = new Rect(i % numberOfItemsPerRow * (cellWidth + 5) + Screen.width / 8, Screen.height / 8 + Mathf.Round(i / numberOfItemsPerRow) * (cellHeight + 5), cellWidth, cellHeight);

                    GUI.DrawTexture(bounds, tex);
                    GUI.Label(text, StringQuest(optionquests[i, 0], optionquests[i, 1], optionquests[i, 2]));
                }

                Rect extra = new Rect(numberOfItemsPerRow * (cellWidth + 5), 1 * (cellHeight + 5), cellWidth, cellHeight);
                GUI.Label(extra, "Difficulty: " + difficulty);

            }
            //draw out the runs changing the texture if selected 
            else if (state==1)
            {
                Rect finish = new Rect(1 * (cellWidth + 5), 2 * cellHeight, cellWidth, cellHeight);
                GUI.DrawTexture(finish, tex);
                GUI.Label(finish, "\n \n \t click to submit runs");

                for (int i = 0; i < numberOfItems; i++)
                {
                    Rect bounds = new Rect(i % numberOfItemsPerRow * (cellWidth + 5), Mathf.Round(i / numberOfItemsPerRow) * (cellHeight + 5), cellWidth, cellHeight);

                    if (selectoptions[i] == false)
                        GUI.DrawTexture(bounds, tex);
                    else
                        GUI.DrawTexture(bounds, tex2);

                    GUI.Label(bounds, "\n \n \t" + StringQuest(optionruns[i, 0], optionruns[i, 1], optionruns[i, 2]));
                }
            }


        }
    }

    /// <summary>
    /// Sets draw to the value given
    /// </summary>
    /// <param name="val">the value to be given to draw</param>
    public void setDraw(bool val)
    {
        draw = val;
    }

    
/// <summary>
/// sets the quest to the quest storage
/// </summary>
/// <param name="type"></param>
/// <param name="init"></param>
/// <param name="goal"></param>
private void CreateQuest(int type,int init,int goal)
    {
        QuestStorage.instance.SetQuest(type, goal, init);
    }
    /// <summary>
    /// Converts the quest to a string so can be read
    /// </summary>
    /// <param name="type"></param>
    /// <param name="init"></param>
    /// <param name="goal"></param>
    /// <returns>string of the quest description</returns>
private string StringQuest(int type, int init, int goal)
    {
        string val = "";
        switch (type)
        {
            case 0:
                val += "Gain this much gold: ";
                val += goal;
                break;
            case 1:
                val += "Defeat all enemies on this level: ";
                val += GlobalFunctions.instance.getLevels()[goal];
                break;
            case 2:
                val += "Get this item: ";
                val += GlobalFunctions.instance.getItems()[goal];
                break;
            case 3:
                val += "Get this score in the minigame: ";
                val += goal;
                break;
            case 4:
                val += "use this many standard attacks in a row";
                val += goal;
                break;
            case 5:
                val += "defeat this person: ";
                string[] people = new string[] {"Paul","Karren","Gladooos"};
                val += people[difficulty];
                break;
            case 6:
                val += "Pacifist run: only kill required enemies";
                break;
            case 7:
                val += "Slaughter run: defeat every enemie";
                break;
            case 8:
                val += "Dragon run: hoard your gold, never spend anything";
                break;
            case 9:
                val += "Monk run: don't equip an item";
                break;
            case 10:
                val += "Social run: speak to everyone";
                break;
            case 11:
                val += "Guadian run: don't let anyone in your team faint";
                break;
            case 12:
                val += "Ironman run: only save on exit";
                break;
            default:
                val += "not implemented";
                break;

        }
        return val;
    }
/// <summary>
/// generate the quests that can be chosen from, using difficulty and randomness 
/// </summary>
/// <param name="difficulty"></param>
private void GenerateQuest(int difficulty)
    {
        for (int i = 0; i < numberOfItems; i++)
        {
            int initprogress = 0;
            int goal = 0;
            int type = Random.Range(0, 5);
            switch (type)
            {
                case 0:
                    initprogress = 0;
                    goal = 100 + difficulty * 2 * 100;
                    break;
                case 1:
                    initprogress = 0;
                    int start = (difficulty / maxDifficulty) * GlobalFunctions.instance.getNumberofLevels() - 2;
                    if (start <= 0)
                        start = Random.Range(1, 3);
                    int end = start + Random.Range(2, 4);
                    if (end > GlobalFunctions.instance.getNumberofLevels())
                        end = GlobalFunctions.instance.getNumberofLevels();
                    goal = Random.Range(start, end);
                    break;
                case 2:
                    initprogress = 0;
                    int star = (difficulty / maxDifficulty) * GlobalFunctions.instance.getNumberofItems() - 2;
                    if (star <= 0)
                        star = Random.Range(1, 3);
                    int en = star + Random.Range(1, 3);
                    if (en > GlobalFunctions.instance.getNumberofItems())
                        en = GlobalFunctions.instance.getNumberofItems();
                    goal = Random.Range(star, en);
                    break;
                case 3:
                    initprogress = 0;
                    goal = 50 + difficulty * 2 * 50; //check easability later
                    break;
                case 4:
                    initprogress = 0;
                    goal = 2 + difficulty * 2;
                    break;
                case 5:
                    initprogress = 0;
                    goal = difficulty;
                    break;
            }

            optionquests[i, 0] = type;
            optionquests[i, 1] = initprogress;
            optionquests[i, 2] = goal;
        }
    }
    /// <summary>
    /// generate the runs which will be the same every time
    /// </summary>
    private void GenerateRuns()
    {
        optionruns[0, 0] = 6;
        optionruns[0, 1] = 1;
        optionruns[0, 2] = 1;

        optionruns[1, 0] = 7;
        optionruns[1, 1] = 0;
        optionruns[1, 2] = 10; //number of battles

        optionruns[2, 0] = 8;
        optionruns[2, 1] = 1;
        optionruns[2, 2] = 1; 

        optionruns[3, 0] = 9;
        optionruns[3, 1] = 1;
        optionruns[3, 2] = 1;

        optionruns[4, 0] = 10;
        optionruns[4, 1] = 0;
        optionruns[4, 2] = 20;//number of chats

        optionruns[5, 0] = 11;
        optionruns[5, 1] = 1;
        optionruns[5, 2] = 1;

        optionruns[6, 0] = 12;
        optionruns[6, 1] = 1;
        optionruns[6, 2] = 1;
    }
}
