﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestStorage : MonoBehaviour {

    public static QuestStorage instance = null;

    public int[,] ActiveQuests { get; set; }
    private int MaxQuests = 10;
    private int QuestPointer = 0;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start () {

        Setup();
		
	}
	/// <summary>
    /// sets the quest storage up filling it with empty quest data
    /// </summary>
    public void Setup()
    {
        ActiveQuests = new int[MaxQuests, 3];
        for (int i = 0; i < MaxQuests; i++)
        {
            ActiveQuests[i, 0] = -1;
        }

        //activequests[number]=[int:type, int:progress,int:goal]
        /*
         should probably make the choice only allow for one of each type.
         type:
            0:get gold //done
            1:defeat all enemies in area //done?
            2:get item  //done
            3:get score in minigame //done?
            4:use x number of basic attacks in a row //done?
            12(5): defeat person x //done

            5:only kill required enemies //done if all optional enemies you can run away from
            6:defeat every enemy and don't run away //done 
            7:never spend money //done
            8:never equip an item //done
            9:speak to everyone //done
            10:don't let anyone die //done
            11:only save and exit when enabled //done



          */
    }
    /// <summary>
    /// set progress with a given type 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="progress"></param>
    public void SetProgressWithType(int type,int progress)
    {
        for (int i=0;i<MaxQuests;i++)
        {
            if(ActiveQuests[i,0]==type)
            {
                ActiveQuests[i,1] = progress;
            }
        }
    }
    /// <summary>
    /// return the progress with a given type
    /// </summary>
    /// <param name="type"></param>
    /// <returns>int progress</returns>
    public int GetProgressWithType(int type)
    {
        for (int i = 0; i < MaxQuests; i++)
        {
            if (ActiveQuests[i,0] == type)
            {
                return ActiveQuests[i,1];
                
            }
        }
        return -1;
    }

    /// <summary>
    /// returns the goal with a given type
    /// </summary>
    /// <param name="type"></param>
    /// <returns>int goal</returns>
    public int GetGoalWithType(int type)
    {
        for (int i = 0; i < MaxQuests; i++)
        {
            if (ActiveQuests[i,0] == type)
            {
                return ActiveQuests[i,2];

            }
        }
        return -1;
    }
    /// <summary>
    /// set progress with position
    /// </summary>
    /// <param name="position"></param>
    /// <param name="progress"></param>
    public void SetProgressWithPosition(int position, int progress)
    {
        ActiveQuests[position,1] = progress;
    }
    /// <summary>
    /// get progress with given position
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public int GetProgressWithPosition(int position)
    {
        return ActiveQuests[position,1];
    }
    /// <summary>
    /// get goal with given position
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public int GetGoalWithPosition(int position)
    {
        return ActiveQuests[position,2];
    }
    /// <summary>
    /// get type with given position
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public int GetTypeWithPosition(int position)
    {
        return ActiveQuests[position,0];
    }
    /// <summary>
    /// set a quest up with a type , goal and progress
    /// </summary>
    /// <param name="type"></param>
    /// <param name="goal"></param>
    /// <param name="initprogress"></param>
    public void SetQuest(int type,int goal, int initprogress)
    {
        ActiveQuests[QuestPointer, 0] = type;
        ActiveQuests[QuestPointer, 1] = initprogress;
        ActiveQuests[QuestPointer, 2] = goal;
        QuestPointer += 1;
    }
    /// <summary>
    /// checks to see if a quest of type exists
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool DoesQuestTypeExist(int type)
    {
        for (int i = 0; i < MaxQuests; i++)
        {
            if (ActiveQuests[i, 0] == type)
                return true;
        }
        return false;

    }
}
