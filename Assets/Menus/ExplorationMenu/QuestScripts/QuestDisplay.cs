﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestDisplay : MonoBehaviour {

    public int index;
    private Text[] component;
    private string label;
    private string progress;
    private string goal;

	// Use this for initialization
	void Start () {
        component = GetComponentsInChildren<Text>();
        label = component[0].text;
        progress = component[1].text;
        goal = component[2].text;
        //set up the inital display
       component[0].text =label+" "+typetostring( QuestStorage.instance.GetTypeWithPosition(index));
        component[1].text = "Progress: " + progresstostring(QuestStorage.instance.GetTypeWithPosition(index),QuestStorage.instance.GetProgressWithPosition(index));
        component[2].text = "Goal: " + goaltostring(QuestStorage.instance.GetTypeWithPosition(index),QuestStorage.instance.GetGoalWithPosition(index));


    }
	
    /// <summary>
    /// converts a type into a string explaining what it is
    /// </summary>
    /// <param name="type"></param>
    /// <returns>string , explanantion</returns>
    private string typetostring(int type)
    {
        string val = "";
        switch (type)
        {
            case 0:
                val += "Gain this much gold: ";
                break;
            case 1:
                val += "Defeat all enemies on this level: ";
                break;
            case 2:
                val += "Get this item: ";
                break;
            case 3:
                val += "Get this score in the minigame: ";
                break;
            case 4:
                val += "use this many standard attacks in a row";
                break;
            case 5:
                val += "defeat this person: ";
                break;
            case 6:
                val += "Pacifist run: only kill required enemies";
                break;
            case 7:
                val += "Slaughter run: defeat every enemie";
                break;
            case 8:
                val += "Dragon run: hoard your gold, never spend anything";
                break;
            case 9:
                val += "Monk run: don't equip an item";
                break;
            case 10:
                val += "Social run: speak to everyone";
                break;
            case 11:
                val += "Guadian run: don't let anyone in your team faint";
                break;
            case 12:
                val += "Ironman run: only save on exit";
                break;
            default:
                val += " ";
                break;

        }
        return val;
    }
    /// <summary>
    /// Converts progress into a more understandable form based on type context
    /// </summary>
    /// <param name="type"></param>
    /// <param name="progress"></param>
    /// <returns> string of progress</returns>
    private string progresstostring(int type, int progress)
    {
        string val = "";
        switch (type)
        {
            case 0:
                val += progress.ToString();
                break;
            case 1:
                if (progress == 1)
                    val += "done";
                else
                    val += "still to do";
                break;
            case 2:
                if (progress == 1)
                    val += "done";
                else
                    val += "still to do";
                break;
            case 3:
                
                val += progress.ToString();
                break;
            case 4:
                val += progress.ToString();
                break;
            case 5:
                if (progress == 1)
                    val += "done";
                else
                    val += "still to do";
                break;
            case 6:
                if (progress == 1)
                    val += "still possible";
                else
                    val += "failed";
                break;
            case 7:
                if (progress == 0)
                    val += "still possible";
                else
                    val += "failed";
                break;
            case 8:
                if (progress == 1)
                    val += "still possible";
                else
                    val += "failed";
                break;
            case 9:
                if (progress == 1)
                    val += "still possible";
                else
                    val += "failed";
                break;
            case 10:
                val += progress.ToString();
                val += "/" + GlobalFunctions.instance.getTalkAmount().ToString();
                break;
            case 11:
                if (progress == 1)
                    val += "still possible";
                else
                    val += "failed";
                break;
            case 12:
                if (progress == 1)
                    val += "still possible";
                else
                    val += "failed";
                break;
            default:
                val += " ";
                break;

        }
        return val;
    }
    /// <summary>
    /// converts goal into string based on conctext type
    /// </summary>
    /// <param name="type"></param>
    /// <param name="goal"></param>
    /// <returns>string of goal</returns>
    private string goaltostring(int type, int goal)
    {
        string val = "";
        switch (type)
        {
            case 0:
                val += goal.ToString();
                break;
            case 1:
                val += GlobalFunctions.instance.getLevels()[goal];
                break;
            case 2:
                val += GlobalFunctions.instance.getItems()[goal];
                break;
            case 3:
                val += goal.ToString();
                break;
            case 4:
                val += goal;
                break;
            case 5:
                string[] people = new string[] { "Paul", "Karren", "Gladooos" };
                val += people[goal];
                break;
            case 6:
                val += "only kill required enemies";
                break;
            case 7:
                val += "defeat every enemie";
                break;
            case 8:
                val += "hoard your gold, never spend anything";
                break;
            case 9:
                val += "don't equip an item";
                break;
            case 10:
                val += "speak to everyone";
                break;
            case 11:
                val += "don't let anyone in your team faint";
                break;
            case 12:
                val += "save causes exit ";
                break;
            default:
                val += " ";
                break;

        }
        return val;
    }
}
