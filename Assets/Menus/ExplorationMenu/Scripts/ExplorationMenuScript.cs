﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// A script to manage the exploration menu that the user can call up at anytime
/// </summary>
public class ExplorationMenuScript : MonoBehaviour {

	public bool menuActive;
	//public bool pseudoKeyPress;
	private GameObject menuBox;
	private PlayerMovement movementScript;

	// Use this for initialization
	void Start () {
        movementScript = FindObjectOfType<PlayerMovement> ();
		menuBox = gameObject.transform.Find ("Panel").gameObject;
		SceneChanger.instance.menuOpen = true;
		//setInactive ();
        if(GlobalFunctions.instance.currentLevel == 0)
        {
            GameObject.FindGameObjectWithTag("MiniGameEnter").GetComponent<Button>().interactable = false;
        }
        else
        {
            GameObject.FindGameObjectWithTag("MiniGameEnter").GetComponent<Button>().interactable = true;
        }
	}

	/// <summary>
	/// Hide menu and renable player movement
	/// </summary>
	private void setInactive() {
		
		movementScript.setCanMove (true);
		menuActive = false;
        menuBox.SetActive(false);
    }

	/// <summary>
	/// When the inventory button is pressed, update <see cref="SceneChanger"/> to show that <see cref="SceneChanger.menuOpen"/>
	/// is now false, and load the item menu  
	/// </summary>
	public void inventPressed() {
		SceneChanger.instance.menuOpen = false;
		SceneChanger.instance.loadLevel ("ItemMenu");
	}

	/// <summary>
	/// When the party button is pressed, update <see cref="SceneChanger"/> to show that <see cref="SceneChanger.menuOpen"/>
	/// is now false, and load the party menu
	/// </summary>
	public void partyPressed() {
		SceneChanger.instance.menuOpen = false;
		SceneChanger.instance.loadLevel ("Party");
	}

    public void questPressed()
    {
        SceneChanger.instance.menuOpen = false;
        SceneChanger.instance.loadLevel("Quest");
    }

    /// <summary>
    /// Placeholder function for when the save button is pressed, to be implemented in later builds
    /// </summary>
    public void savePressed() {
        if(QuestStorage.instance.DoesQuestTypeExist(12))
        {
            PlayerData.instance.data.Save();
            SceneChanger.instance.menuOpen = false;
            SceneChanger.instance.loadLevel("mainmenu1");

        }
        else
            PlayerData.instance.data.Save();
    }

	/// <summary>
    /// Initiates town mode scene
	/// </summary>
	public void tradePressed() {
        SceneChanger.instance.menuOpen = false;
        if (GlobalFunctions.instance.currentLevel<5)
        SceneManager.LoadScene("TownMode", LoadSceneMode.Additive);
        else
            SceneManager.LoadScene("TownMode2", LoadSceneMode.Additive);
    }

    /// <summary>
    /// Initiates miniGame
	/// </summary>
	public void miniGamePressed()
    {
        
        SceneChanger.instance.menuOpen = false;
        GlobalFunctions.instance.player.SetActive(false);
        SceneManager.LoadScene("MiniGame");
    }

    /// <summary>
    /// When the exit button is pressed, update <see cref="SceneChanger"/> to show that <see cref="SceneChanger.menuOpen"/>
    /// is now false, and go back to the main menu
    /// </summary>
    public void exitPressed() {
		SceneChanger.instance.menuOpen = false;
		SceneChanger.instance.loadLevel ("mainmenu1");
	}
}
