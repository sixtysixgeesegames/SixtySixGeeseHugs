﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScript : MonoBehaviour {

/// <summary>
/// stops the credits and returns to the main menu
/// </summary>
public void End()
    {
        Destroy(this.gameObject);
        SceneChanger.instance.loadLevel("mainmenu1", new Vector2(0, 0));
    }
}
